<?php

/**
 * Form that allows manual purging of urls
 * and to provide values independent of the views
 */
function varnish_simple_purge_form($form_id, $form_state) {
  $form = array();
  $form['about'] = array(
    '#markup' => t('This page allows you purge individual URLs from the Varnish cache.'),
  );

  $homepage = variable_get('site_frontpage', '');

  global $base_url;
  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('Enter the relative url'),
    '#size' => 60,
    '#maxlength' => 200,
    '#field_prefix' => $base_url . '/',
    '#field_suffix' => t('<i>Note: Homepage is <strong>/%home</strong></i>', array('%home' => $homepage)),
  );
  // Submit
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Purge'),
  );
  return $form;
}

/**
 * Validation function for views cache global settings form
 */
function varnish_simple_purge_form_validate(&$form, &$form_state) {
  $values = $form_state['values'];
  $url = trim($values['url'], '/');
  // Accept only relative url
  if (strpos($url, '://') !== FALSE) {
    form_set_error($form_state['values']['url'], 'Please only enter the relative part of the url. E.g. /home');
  }
  if (!parse_url($url)) {
    form_set_error($form_state['values']['url'],
      'Unable to parse url, please make sure you enter the relative url in the correct format.');
  }
}

/**
 * Submit function to save purge URL from Varnish
 */
function varnish_simple_purge_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];
  $url = trim($values['url'], '/');
  $parsed = drupal_parse_url($url);
  $homepage = variable_get('site_frontpage', '');
  // Construct the Varnish purge pattern
  $purge_pattern = "^/" . $parsed['path'] . "$";
  // Purge the pattern
  varnish_simple_purge_pattern($purge_pattern);
  // For homepage, additionally purge the front page
  if ($homepage == $parsed['path']) {
    varnish_simple_purge_pattern("^/$");
  }
  drupal_set_message(t('Purged %url from Varnish cache.', array('%url' => $parsed['path'])));
}